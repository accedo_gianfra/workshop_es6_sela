var colors = require('colors');

module.exports = function()  {
  const allColors = [colors.red, colors.green, colors.blue, colors.yellow];
  const randomColor = allColors[Math.floor(Math.random()*allColors.length)];
  console.log(randomColor(...arguments));
}
