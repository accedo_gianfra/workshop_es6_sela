const noop = () => {}

function getAddress() {
  return {
    city: 'Madrid',
    state: 'SPAIN',
    zip: 28014,
    coords: {
      lat: 40.776608,
      long: -111.920485,
    },
  }
}

function getNumbers() {
  return [1, 2, 3, 4, 5, 6]
}

function getNestedNumbers() {
  return [1, 2, [3, 4, [5, 6]]]
}

xtest('can be used to pull apart objects', () => {
  // Using destructuring, call `getAddress()` and create a 'city', 'state' and 'zip' variable.
  // const address = getAddress();
  // const city = address.city;
  // const state = address.state;
  // const zip = address.zip;
  expect(city).toBe('Madrid')
  expect(state).toBe('SPAIN')
  expect(zip).toBe(28014)
})

xtest('sets missing values to undefined', () => {
  // Using destructuring, call `getAddress()` and create an 'address' variable.
  expect(address).toBeUndefined()
})

xtest('can alias destructured variables', () => {
  // Using destructuring, call `getAddress()` and pull the city, state and zip out, and alias them to c, s, z, respectively
  expect(c).toBe('Madrid')
  expect(s).toBe('SPAIN')
  expect(z).toBe(28014)
  expect(() => noop(city)).toThrow()
  expect(() => noop(state)).toThrow()
  expect(() => noop(zip)).toThrow()
})

xtest('can destructure nested variables', () => {
  // Using destructuring, call `getAddress()` and create `lat` and `long` variables.
  expect(lat).toBe(40.776608)
  expect(long).toBe(-111.920485)
  expect(() => noop(coords)).toThrow()
})

xtest('can be used to pull apart arrays', () => {
  // Call getNumbers and pull the first value out as `one` and the second as `two`
  expect(one).toBe(1)
  expect(two).toBe(2)
})

xtest('can skip indexes in arrays', () => {
  // Call getNumbers and pull the first value out as `one` and the third as `three`
  expect(one).toBe(1)
  expect(three).toBe(3)
  expect(() => noop(two)).toThrow()
})

xtest('can reach nested arrays', () => {
  // Call getNestedNumbers and pull the first value out as `one`, the 3 as `three` and 6 as `sixth`.
  expect(one).toBe(1)
  expect(three).toBe(3)
  expect(six).toBe(6)
})