# workshop_ES6_SELA

## Install:
- clone repo
- `npm install`

## Run:

### Arrow function
start: `npm run start:arrow`

test: `npm run test:arrow`

### Spread syntax
start: `npm run start:spread`

test: `npm run test:spread`

### Destructuring
start: `npm run start:des`

test: `npm run test:des`

## Useful Link:
[workshop ES6](https://www.youtube.com/watch?v=t3R3R7UyN2Y&t=4844s&list=PLV5CVI1eNcJgNqzNwcs4UKrlJdhfDjshf&index=16)

[node.green](https://node.green/)

[ECMAScript proposals](https://github.com/tc39/proposals)