var log = require('./../utils/log.js');
// Spread operator: turns the items of an iterable
// into arguments of a function call or into elements of an Array.

function spreadFunctionCall() {
  // TODO: return the maximum value in an array of integers
  // use spread operator and Math.max() in refactoring
  const arr = [5, 6, 8, 4, 9]
  // It is common to use Function.prototype.apply in cases
  // where you want to use the elements of an array as arguments to a function.
  return Math.max.apply(null, arr)
}
// log(spreadFunctionCall());

function concatArrays() {
  // TODO: flatten nested arrays of arbitrary levels of nesting
  // arr can be typically like this: [8, 9, [6, [5, [7], [45, 34, [2]]]]]
  // output shold be [8, 9, 6, 5, 7, 45, 34, 2]
  // use spread operator in place of Array.prototype.concat()
  const arr = [8, 9, [6, [5, [7], [45, 34, [[[2]]], [[[[[[[[7]]]]], 90]]]]]]]
  return flatter(arr)

  function flatter(arg) {
    return arg.reduce((acc, item) => {
      if (Array.isArray(item)) {
        return acc.concat(flatter(item))
      }
      return acc.concat([item])
    }, [])
  }
}
log(concatArrays());

// object spread syntax
function mergeObjects() {
  // refactor to object spread
  const obj1 = {
    a: 'a from obj1',
    b: 'b from obj1',
    c: 'c from obj1',
    d: {
      e: 'e from obj1',
      f: 'f from obj1',
    },
  }
  const obj2 = {
    b: 'b from obj2',
    c: 'c from obj2',
    d: {
      g: 'g from obj2',
      h: 'g from obj2',
    },
  }
  return Object.assign({}, obj1, obj2)
}
// log(mergeObjects());

// COPY AN ARRAY
function copyArray() {
  var first = [1, 2, 3];
  var second = first.slice();
  second.push(4);
  // log('first array: ', first);
  // log('second array: ', second);
}
// copyArray();

var parts = ['shoulders', 'knees'];
var lyrics = ['head', ...parts, 'and', 'toes'];
// ["head", "shoulders", "knees", "and", "toes"]

// The Spread Operator vs Object.assign()
const obj = { a: 1, b: 2 };
const obj2 = { c: 3 };

const assignedObject = Object.assign({}, obj, obj2);

const spreadObj = {
  ...obj,
  ...obj2
}
// log(assignedObject) // this logs {a: 1, b: 2, c: 3}
// log(spreadObj) // this logs {a: 1, b: 2, c: 3}

// both result in the same object

const original = { a: { b: 1 } };

const falseCopy = { ...original };

falseCopy.a.b = 2;

// log(falseCopy) // logs {a: {b: 2}}
// log(original) // also logs {a: {b: 2}}

// Under the hood, Babel is transpiling the ... to Object.assign so shallow copy if the obj