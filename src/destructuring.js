var log = require('./../utils/log.js');

// it makes possible to unpack values from arrays, or properties from objects, into distinct variables.

function getAverage() {
  // refactor with object destructuring
  const obj = {x: 3.6, y: 7.8, z: 4.3}

  return Math.floor((obj.x + obj.y + obj.z) / 3.0)
}
// log(getAverage())

function getAvgTemp() {
  // refactor with nested destructuring
  const weather = {
    location: 'Toronto',
    unit: 'Celsius',
    today: {max: 2.6, min: -6.3},
    tomorrow: {max: 3.2, min: -5.8},
  }
  const maxToday = weather.today.max
  const minToday = weather.today.min

  const maxTomorrow = weather.tomorrow.max
  const minTomorrow = weather.tomorrow.min

  return {
    max: (maxToday + maxTomorrow) / 2.0,
    min: (minToday + minTomorrow) / 2.0,
    unit: weather.unit,
  }
}
// log(getAvgTemp())

function getFirstTwo() {
  // refactor with array destructuring
  const arr = [0, 1, 2, 3, 4, 5, 6, 7]
  const firstItem = arr[0]
  const secondItem = arr[1]

  return {
    firstItem: firstItem,
    secondItem: secondItem,
  }
}
// log(getFirstTwo())

function getElements() {
  // returns 1st, 2nd and fifth element from an array
  // refactor with skipped destructuring for arrays
  const arr = [0, 1, 2, 3, 4, 5, 6, 7]
  const first = arr[0]
  const second = arr[1]
  const fifth = arr[4]

  return {
    first: first,
    second: second,
    fifth: fifth,
  }
}
// log(getElements())

function getSecondItem() {
  // refactor with nested destructuring of arrays
  const food = [
    ['carrots', 'beans', 'peas', 'lettuce'],
    ['apple', 'mango', 'orange'],
    ['cookies', 'cake', 'pizza', 'chocolate'],
  ]
  const firstItem = food[0][1]
  const secondItem = food[1][1]
  const thirdItem = food[2][1]

  return {
    first: firstItem,
    second: secondItem,
    third: thirdItem,
  }
}
// log(getSecondItem())

function nestedArrayAndObject() {
  // refactor this to a single line of destructuring...
  const info = {
    company: 'Accedo',
    employees: {
      name: 'JS team',
      devs: [
        {name: 'Luis', surname: 'Mesejo'},
        {name: 'Oscar', surname: 'Teston'},
        {name: 'Victor', surname: 'Gonzalez'},
        {name: 'Javier', surname: 'Pelado'},
      ],
    },
  }
  const company = info.company;
  const team = info.employees.name;
  const employee = info.employees.devs[3];
  const employeSurname = employee.surname;
  const employeName = employee.name;

  return `${employeName} (${employeSurname}) is an employee of ${team} in "${company}"`
}
// log(nestedArrayAndObject())

function defaultValues() {
  const bench = {type: 'Piano', adjustable: false }
  const legCount = bench.legs === undefined ? getDefaultLegCount() : bench.legs

  return legCount

  function getDefaultLegCount() {
    return 4
  }
}
// log(defaultValues())

function ontoAnObject() {
  // refactor this to destructuring
  const array = [1, 2, 3, 4, 5, 6];
  const object = {};
  object.one = array[0];
  object.two = array[1];
  object.three = array[2];
  object.rest = array.slice(3);

  return object;
}
// log(ontoAnObject())













// MORE
// Rest operator: collects the remaining items of an iterable into an Array
// and is used for rest parameters and destructuring.
function restString() {
  // what does this return?
  const greeting = 'Hello world'
  const [h, e, l, l2, o, space, ...splitGreeting] = greeting
  return splitGreeting
}

function spreadString() {
  return join('--', 'PayPal')

  function join(delimiter, string) {
    return [...string].join(delimiter)
  }
}