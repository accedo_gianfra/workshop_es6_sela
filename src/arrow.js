var log = require('./../utils/log.js');

// Two factors influenced the introduction of arrow functions: shorter functions and no existance of this keyword.

function multiArgument() {
  // refactor to an arrow function
  const divide = function(a, b) {
    return a / b
  }
  return divide(40, 10)
}
// log(multiArgument())

function noArgument() {
  // refactor to an arrow function
  const getFive = function() {
    return 5
  }
  return getFive()
}
// log(noArgument())

function singleArgument() {
  // refactor to an arrow function
  const identity = function(i) {
    return i
  }
  return identity(350)
}
// log(singleArgument())

function withObject() {
  // refactor to an arrow function
  const getObject = function(favoriteCandy) {
    return {favoriteCandy}
  }
  return getObject('twix')
}
// log(withObject())

var arr = ['hammer', 'nails', 'pizza', 'test'];

var items = arr.map(function(item) {
  return item.length;
});
// log('items:', items);

// Fat arrows change how `this` is handled.
// Before...
// In ES5, `bind()` or var that = this; are necessary as functions
// create their own `this`. We need to store the parent `this` in
// a variable that can be referenced in the callback or take care of binding ourselves.

// ES6 Arrows instead bind `this` to the immediate enclosing lexical scope:
// An arrow function does not have its own this; the this value of the enclosing lexical context is used

function Person() {
  this.age = 40;
  this.name = 'Gololo';
}
Person.prototype.checkName = function(names) {
  return names.find(function(name) {
    return name === this.name;
  }.bind(this))
}
var p = new Person();
var names = ['Nacho', 'Javier', 'Gololo', 'Oscar'];
// log(p.checkName(names));



function context() {
  var population = {
    unit: 'Million',
    data: [
      {city: 'Tokyo', value: 37.833},
      {city: 'Sanghai', value: 22.991},
      {city: 'New Delhi', value: 24.953},
    ],
    display() {
      // TODO: refactor with arrow function
      return this.data.map(function populationStringMapper(element) {
        return `Population in ${element.city} is ${element.value} ${this.unit}`
      }) // passing `this` as second argument to bind properly
    },
  }
  return population.display()
}
// log(context());







// MORE:
const simpleAction = (data)  => ({
   type: 'SIMPLE_ACTION',
   payload: data
 })

// does not contain its own this
// --------------------------------------------------
var obj = {
  i: 10,
  b: () => log('b: ', this.i, this),
  c: function() {
    log('c: ', this.i, this);
  }
}

// obj.b(); // prints undefined, Window {...} (or the global object)
// obj.c(); // prints 10, Object {...}

// Arrow functions do not have a prototype.
var func = () => {};
// log(func.prototype); // undefined
// --------------------------------------------------



