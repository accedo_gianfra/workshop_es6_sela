function multiArgumentSOLUTION() {
  const divide = (a, b) => a / b
  return divide(40, 10)
}

function noArgumentSOLUTION() {
  const getFive = () => 5
  return getFive()
}

function singleArgumentSOLUTION() {
  const identity = i => i
  return identity(350)
}

function withObjectSOLUTION() {
  // refactor to an arrow function
  const getObject = favoriteCandy => ({favoriteCandy})
  return getObject('twix')
}

function Person() {
  this.age = 40;
  this.name = 'Gololo';
}
Person.prototype.checkName = function(names) {
  return names.find(name => {
    return name === this.name;
  })
}
var p = new Person();
var names = ['Nacho', 'Javier', 'Gololo', 'Oscar'];

function context() {
  var population = {
    unit: 'Million',
    data: [
      {city: 'Tokyo', value: 37.833},
      {city: 'Sanghai', value: 22.991},
      {city: 'New Delhi', value: 24.953},
    ],
    display() {
      // TODO: refactor with arrow function
      return this.data.map(element => {
        return `Population in ${element.city} is ${element.value} ${this.unit}`
      }) // passing `this` as second argument to bind properly
    },
  }
  return population.display()
}