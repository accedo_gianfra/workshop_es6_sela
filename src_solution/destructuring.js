function getAverageSOLUTION() {
  const obj = {x: 3.6, y: 7.8, z: 4.3}
  const {x, y, z} = obj
  return Math.floor((x + y + z) / 3.0)
}
// log(getAverageSOLUTION())

function getAvgTempSOLUTION() {
  const weather = {
    location: 'Toronto',
    unit: 'Celsius',
    today: {max: 2.6, min: -6.3},
    tomorrow: {max: 3.2, min: -5.8},
  }
  const {
    unit,
    today: {max: maxToday, min: minToday},
    tomorrow: {max: maxTomorrow, min: minTomorrow},
  } = weather

  return {
    max: (maxToday + maxTomorrow) / 2.0,
    min: (minToday + minTomorrow) / 2.0,
    unit: unit,
  }
}
// log(getAvgTempSOLUTION())

function getFirstTwoSOLUTION() {
  const arr = [0, 1, 2, 3, 4, 5, 6, 7]
  const [first, second] = arr

  return {
    first: first,
    second: second,
  }
}
// log(getFirstTwoSOLUTION())

function getElementsSOLUTION() {
  const arr = [0, 1, 2, 3, 4, 5, 6, 7]
  const [first, second, , , fifth] = arr

  return {
    first: first,
    second: second,
    fifth: fifth,
  }
}
// log(getElementsSOLUTION())

function getSecondItemSOLUTION() {
  const food = [
    ['carrots', 'beans', 'peas', 'lettuce'],
    ['apple', 'mango', 'orange'],
    ['cookies', 'cake', 'pizza', 'chocolate'],
  ]
  const [[, firstItem], [, secondItem], [, thirdItem]] = food

  return {
    first: firstItem,
    second: secondItem,
    third: thirdItem,
  }
}

function defaultValuesSOLUTION() {
  const bench = {type: 'Piano', adjustable: false}
  const {legs: legCount = getDefaultLegCount()} = bench
  return legCount

  function getDefaultLegCount() {
    return 4
  }
}
// log(defaultValuesSOLUTION())

function ontoAnObjectSOLUTION() {
  const array = [1, 2, 3, 4, 5, 6]
  const object = {};
  [
    object.one,
    object.two,
    object.three,
    ...object.rest
  ] = array
  return object
}